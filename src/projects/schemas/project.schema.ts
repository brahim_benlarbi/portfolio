import * as mongoose from 'mongoose';

export const ProjectSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'is required']
  },
  description: {
    type: String,
    required: [true, 'is required']
  },
  imagePath: String,
  client: {
    type: String,
    required: [true, 'is required']
  },
});
