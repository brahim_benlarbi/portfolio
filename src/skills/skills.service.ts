import { Injectable, Logger, HttpStatus, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Skill } from './interfaces/skill.interface';
import { SkillDto } from './dto/skill.dto';

@Injectable()
export class SkillsService {
  constructor(
    @InjectModel('Skill') private readonly skillModel: Model<Skill>,
  ) {}

  async getAll(): Promise<Skill[]> {
    return await this.skillModel.find().exec();
  }

  async create(skill: SkillDto): Promise<Skill> {
    const newSkill = await new this.skillModel(skill);
    return newSkill.save();
  }

  async getById(id: string): Promise<Skill> {
    try {
      return this.skillModel.findById(id).exec();
    } catch (err) {
      Logger.error(err.message, err, 'SkillsService');
      throw new HttpException('Skill not found', HttpStatus.NOT_FOUND);
    }
  }

  async update(id: string, uSkill: SkillDto): Promise<Skill> {
    return await this.skillModel
      .findByIdAndUpdate(id, uSkill, { new: true })
      .exec();
  }

  async delete(id: string): Promise<Skill> {
    return await this.skillModel.findByIdAndRemove(id);
  }
}
