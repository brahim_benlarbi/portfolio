import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  Logger,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ProjectDto } from './dto/project.dto';
import { Project } from './interfaces/project.interface';
import { ApiUseTags, ApiImplicitParam } from '@nestjs/swagger';

@ApiUseTags('projects')
@Controller('projects')
export class ProjectsController {
  constructor(private projects: ProjectsService) {}

  @Get()
  getProjects(): Promise<Project[]> {
    return this.projects.getAll();
  }

  @Post()
  async createProject(@Body() cProject: ProjectDto): Promise<Project> {
    try {
      return await this.projects.create(cProject);
    } catch (err) {
      Logger.error(err.message, err, 'ProjectsController');
      throw new HttpException(err.message, HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  @ApiImplicitParam({ name: 'id' })
  @Get(':id')
  getProject(@Param('id') id: string): Promise<Project> {
    return this.projects.getById(id);
  }

  @ApiImplicitParam({ name: 'id' })
  @Put(':id')
  updateProject(
    @Param('id') id: string,
    @Body() uProject: ProjectDto,
  ): Promise<Project> {
    return this.projects.update(id, uProject);
  }

  @ApiImplicitParam({ name: 'id' })
  @Delete(':id')
  deleteProject(@Param('id') id: string): Promise<Project> {
    return this.projects.delete(id);
  }
}
