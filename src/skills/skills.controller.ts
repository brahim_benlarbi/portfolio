import {
  Controller,
  Get,
  Post,
  Body,
  Logger,
  HttpException,
  HttpStatus,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam } from '@nestjs/swagger';
import { SkillsService } from './skills.service';
import { Skill } from './interfaces/skill.interface';
import { SkillDto } from './dto/skill.dto';

@ApiUseTags('skills')
@Controller('skills')
export class SkillsController {
  constructor(private skills: SkillsService) {}

  @Get()
  async getSkills(): Promise<Skill[]> {
    return this.skills.getAll();
  }

  @Post()
  async createSkill(@Body() cSkill: SkillDto): Promise<Skill> {
    try {
      return await this.skills.create(cSkill);
    } catch (err) {
      Logger.error(err.message, err, 'SkillsController');
      throw new HttpException(err.message, HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

  @ApiImplicitParam({ name: 'id' })
  @Get(':id')
  getSkill(@Param('id') id: string): Promise<Skill> {
    return this.skills.getById(id);
  }

  @ApiImplicitParam({ name: 'id' })
  @Put(':id')
  updateSkill(
    @Param('id') id: string,
    @Body() skill: SkillDto,
  ): Promise<Skill> {
    return this.skills.update(id, skill);
  }

  @ApiImplicitParam({ name: 'id' })
  @Delete(':id')
  deleteProject(@Param('id') id: string): Promise<Skill> {
    return this.skills.delete(id);
  }
}
