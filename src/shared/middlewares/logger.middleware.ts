import { Injectable, NestMiddleware, Logger } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    Logger.log('Portfolio API ' + req.method + ' Request to: ' + req.originalUrl, 'LoggerMiddleware' );
    next();
  }
}
