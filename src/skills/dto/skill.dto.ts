import { ApiModelProperty } from '@nestjs/swagger';

export class SkillDto {
  @ApiModelProperty()
  readonly name: string;
  @ApiModelProperty()
  readonly description: string;
  @ApiModelProperty()
  readonly icon?: string;
  @ApiModelProperty()
  readonly percentage: number;
}
