export interface Skill {
  id?: string;
  name: string;
  description: string;
  icon: string;
  percentage: number;
}
