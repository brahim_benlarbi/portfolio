import * as mongoose from 'mongoose';

export const SkillSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'is required']
  },
  description: {
    type: String,
    required: [true, 'is required']
  },
  icon: String,
  percentage: {
    type: Number,
    required: [true, 'is required']
  },
});