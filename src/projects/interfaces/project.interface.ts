export interface Project {
  id?: string;
  name: string;
  description: string;
  imagePath?: string;
  client: string;
}
