import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { ProjectDto } from './dto/project.dto';
import { Project } from './interfaces/project.interface';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectModel('Project') private readonly projectModel: Model<Project>,
  ) {}

  async getAll(): Promise<Project[]> {
    return await this.projectModel.find().exec();
  }

  async getById(id: string): Promise<Project> {
    try {
      return await this.projectModel.findById(id).exec();
    } catch (err) {
      Logger.error(err.message, err, 'ProjectsService');
      throw new HttpException('Project not found', HttpStatus.NOT_FOUND);
    }
  }

  async create(project: Project): Promise<Project> {
    const newProject = await new this.projectModel(project);
    return newProject.save();
  }

  async update(id: string, uProject: ProjectDto): Promise<Project> {
    return await this.projectModel
      .findByIdAndUpdate(id, uProject, { new: true })
      .exec();
  }

  async delete(id: string): Promise<Project> {
    return await this.projectModel.findByIdAndRemove(id);
  }
}
