import 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger } from '@nestjs/common';

const port = process.env.PORT || 8080;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.enableCors();
  const sdOptions = new DocumentBuilder()
    .setTitle('Portfolio Api')
    .setDescription('Potfolio descrition Api')
    .setVersion('0.0.1')
    // .addTag('projects')
    .build();
  const sDocument = SwaggerModule.createDocument(app, sdOptions);
  SwaggerModule.setup('api', app, sDocument);
  await app.listen(port);
  Logger.log('Server is running on http://localhost:' + port, 'Bootstrap');
  Logger.log('Potfolio API is running on http://localhost:' + port + '/api', 'Bootstrap');
}
bootstrap();
