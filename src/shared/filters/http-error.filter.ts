import {
  Catch,
  ExceptionFilter,
  HttpException,
  ArgumentsHost,
  Logger,
} from '@nestjs/common';

@Catch()
export class HttpErrorFilter implements ExceptionFilter {
  catch(ex: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const req = ctx.getRequest();
    const res = ctx.getResponse();
    const status = ex.getStatus();
    const message = ex.message.error || ex.message || 'Generic Error';

    const error = {
      code: status,
      path: req.url,
      method: req.method,
      message: message,
    };

    Logger.error(req.method + ' ' + req.url, ex.stack, 'HttpErrorFilter');

    res.status(status).json(error);
  }
}
