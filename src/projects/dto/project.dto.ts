import { ApiModelProperty } from '@nestjs/swagger';

export class ProjectDto {
  @ApiModelProperty()
  readonly name: string;
  @ApiModelProperty()
  readonly description: string;
  @ApiModelProperty()
  readonly imagePath?: string;
  @ApiModelProperty()
  readonly client: string;
}
